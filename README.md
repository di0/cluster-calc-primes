# README #

For establishing baseline performance and up-operation during Edge Cluster research (target: Raspberry Pi Platform), as well as providing a metric for measuring the impact of both hardware and software cluster improvements.

## Tested with ##

* 1 x Raspberry Pi 3B+
* 4 x Raspberry Pi Zero 1.3
* 1 x ClusterHAT v2.5

### Cluster Benchmark ###

Calculates primes to the specified number on a cluster with mpi running on all nodes, and with all nodes having both of the below python scripts existing in the root of the pi user's home folder.

Example usage:

`mpiexec --host p1,p2,p3,p4 python3 cluster-calc-primes.py 10000`

cluster-calc-primes.py 

* is modeled upon this: https://github.com/joshjerred/mpi4py-with-multiprocessing-Check-for-primes/blob/main/primeMP.py

### Standalone System Benchmark ###

Calculates primes to the specified number on a standalone (non-clustered) system. This is strictly for comparison to show the performance of a cluster to, say, a laptop. The calculation approach is the same, just using all CPU cores of a single system, instead of using all CPU cores of a cluster.

The reason I have a separate script here, instead of using the same script on the cluster and the standalone tests, is that Windows+python+MPI don't work good. So, technically, this isn't a 1:1 comparison, since the code is functionally the same, but is not exactly the same.

Example usage:

`mpiexec --host p1 python3 cluster-calc-primes.py 10000` 

^^^ doesn't work on windows, but should

`standalone-calc-primes.py 10000`

^^^ does work on windows

standalone-calc-primes.py

* is modeled upon this: https://www.the-diy-life.com/can-my-water-cooled-raspberry-pi-cluster-beat-my-macbook/#:~:text=one%20of%20those.-,Edit,-%E2%80%93%20Multi-process%20Test
